const { src, dest, watch, parallel, series } = require('gulp');
const scss = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const uglify = require('gulp-uglify-es').default;
const browserSync = require('browser-sync').create();
const autoprefixer = require('gulp-autoprefixer');
var clean = require('gulp-clean');

function cleaning() {
    return src('dist')
    .pipe(clean());
}

function scripts() {
    return src('app/js/*.js')
    .pipe(concat('main.min.js'))
    .pipe(uglify())
    .pipe(dest('app/dist/js'))
    .pipe(browserSync.stream())
}

function styles() {
    return src('app/scss/*.scss')
        .pipe(autoprefixer())
        .pipe(concat('style.min.css'))
        .pipe(scss({ outputStyle: 'compressed' }))
        .pipe(dest('app/dist/css'))
        .pipe(browserSync.stream())
}

function watching(){
    watch(['app/scss/*.scss'], styles);
    watch(['app/js/*.js'], scripts);
    watch(['app/*.html']).on('change', browserSync.reload);
}

function browserSyncing(){
    browserSync.init({
        server: {
            baseDir: "app/"
        }
    })
};

function bulid() {
    return src([
        'app/dist/css/style.min.css',
        'app/dist/js/main.min.js',
        'app/*.html',
    ], {base : 'app'})
    .pipe(dest('dist'))
}

exports.cleaning = cleaning;
exports.styles = styles;
exports.scripts = scripts;
exports.watching = watching;
exports.browserSyncing = browserSyncing;
exports.default = parallel(styles, scripts, browserSyncing, watching);
exports.build = series(cleaning, styles, scripts, bulid);